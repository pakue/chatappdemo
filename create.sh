group=pakuedemoapp;plan=pakuedemoapp;app=pakuechat

az group create -l japanwest -n $group
az appservice plan create -g $group --sku S1 -n $plan
az webapp create -n $app -p $plan -g $group -r "node|8.1" -l
az webapp config set -n $app -g $group --web-sockets-enabled true

url=$(az webapp deployment source config-local-git -n $app -g $group --query url --output tsv)
git remote remove azure
git remote add azure $url

