
# ５分でできるチャットサーバ作成デモ

## 事前準備

- Microsoft Azureのアカウントを作成（無料枠でデモ出来ます）
- CloudShellを立ち上げられるか確認する
- CloudShell内でこのリポジトリをCloneする
- create.shの１行目にある各種名前を事前に変更しておきます(そのまま使うとURLが重複する可能性があります)


## デモ手順

- ポータルが表示されている状態から始めます
- CloudShellを開け、リポジトリのディレクトリに入ります
- ./create.sh を実行します
- ポータルでできあがったリソースを順番にゆっくり話しながら開いていきます
- 最後にCloudShellの様子を見ながら、デプロイが完了していたらWebAppを開きます(URLをコピーするため）
- ステータスがRunningになっていたら、CloudShellにてgit push azure masterと打ち込みデプロイをします
- デプロイ状況を見ながらアクセスするURLをコピーし、QRコードを作成してくれるサービスを使って会場にURLを伝えます

## コツ

- CloudShellは20分のアクセスが無いと環境が切断されるので、待ち時間が長い時は気をつけて下さい
- create.shの中身をくどくど説明してはいけません。5分で終わりません
- なるべく早くCloudShellでCreate.shを実行しましょう
- 会場の人数が100名以上の場合は作成するAppServiceのプランを上げましょう
- 人数が少ない場合でもAppServiceの無料プランではなく、有償プランを使うことをお勧めします
- もしデプロイするところまで自動化する場合はcreate.shの最後にgit pushを書いてしまいましょう
- 何度もデモをする場合はURLが使えなくなることがあるので名前を変えましょう
- もしソースをgithubなどに上げられる場合はCloudShellからソースを上げないでやるのも可能ですが、  
それだとライブ感が減るのでgit pushくらいは打つのをお勧めします
- Azureのポータルにリソースグループの一覧をピン留しておくとスムーズに作成されたリソースを見ていけます

## 技術的な面の解説

- ソースコードはSocket.ioのChatデモをベースにしました。（一部改変してます）
    - 元index.jsをappjsに変更
    - app.js内のsocket.ioのrequireを相対パスからライブラリ名へ
    - app.js内のtypingに関係するメソッドをコメントアウト
    - package.jsonにsocket.ioを追加
    - package.jsonのstartをindex.jsからapp.jsへ変更
    - public/index.htmlにviewportを追加
- create.shではAzure CLIを使い、各種リソースを作成しています。使用しているのは東日本リージョンです。
- AppServiceのS1というプランを使っています。デモ後にすぐに環境を消すと数円で済みます。  
https://azure.microsoft.com/ja-jp/pricing/details/app-service/windows/
- 作業する環境ではCloudShellを使っています。色々なツールがインストール済みなのでデモやちょっとした作業に最適です。  
https://docs.microsoft.com/ja-jp/azure/cloud-shell/overview
- QRコードの作成は以下のサービスを使いました。  
https://www.the-qrcode-generator.com/
- QRコードを参加者に読み込んでもらいます。iOSの場合は標準のカメラアプリでQRコードを写すとリンクを表示できます。  
Androidの場合はカメラアプリでQRコードを表示後にホームボタンを長押しすると「画像の検索」がでリンクを開けます。

## 元ネタ

このデモは以下のURLで公開のサンプルをベースにしておりますので、こちらでまずは勉強しましょう  
https://docs.microsoft.com/ja-jp/azure/app-service/scripts/app-service-cli-deploy-local-git?toc=%2fcli%2fazure%2ftoc.json
